# frozen_string_literal: true

require 'minitest/autorun'
require_relative '../lib/variables'

describe Variables do
  describe 'with circular dependency in variable set' do
    before do
      @vars = Variables.new(
        [
          { key: '_A1', value: '$B' },
          { key: '_A10', value: '10' },
          { key: 'B', value: '${A}-$C' },
          { key: 'C', value: '$_A1' }
        ]
      )
    end

    describe '#to_runner_variables' do
      let(:runner_variables) { proc { @vars.to_runner_variables } }

      describe 'with feature flag disabled' do
        before do
          $var_expansion_feature_flag = false
        end

        it 'must not raise' do
          assert_kind_of Array, runner_variables.call
        end
      end

      describe 'with feature flag enabled' do
        before do
          $var_expansion_feature_flag = true
        end

        it 'must raise CyclicVariableReference' do
          _(runner_variables).must_raise CyclicVariableReference
        end
      end
    end
  end

  describe 'with valid variable set' do
    before do
      @vars = Variables.new(
        [
          { key: 'C', value: '${A}-%B%' },
          { key: 'A', value: '$B', masked: true },
          { key: 'B', value: '1' },
          { key: 'D', value: '$BUILD_DIR' },
          { key: 'E', value: 'key%variable%%variable2%' },
          { key: 'F', value: '$G' },
          { key: 'G', value: '$H' }
        ]
      )
    end

    describe '#to_runner_variables' do
      let(:runner_variables) { @vars.to_runner_variables }

      describe 'with feature flag disabled' do
        before do
          $var_expansion_feature_flag = false
        end

        it 'must respond with pre-existing order (C A B D E F G)' do
          _(runner_variables.length).must_equal 7
          _(runner_variables.map { |v| v[:key] }).must_equal %w[C A B D E F G]
        end

        it 'must not mark C variable as masked' do
          _(runner_variables.find { |v| v[:key] == 'C' }.fetch(:masked)).must_equal false
        end
      end

      describe 'with feature flag enabled' do
        before do
          $var_expansion_feature_flag = true
        end

        it 'must respond with topologically sorted order (B A C D E G F)' do
          _(runner_variables.length).must_equal 7
          _(runner_variables.map { |v| v[:key] }).must_equal %w[B A C D E G F]
        end

        it 'must expand A variable' do
          _(runner_variables.find { |v| v[:key] == 'A' }.fetch(:value)).must_equal '1'
        end

        it 'must expand C variable' do
          _(runner_variables.find { |v| v[:key] == 'C' }.fetch(:value)).must_equal '1-1'
        end

        it 'must mark C variable as masked' do
          _(runner_variables.find { |v| v[:key] == 'C' }.fetch(:masked)).must_equal true
        end

        it 'must not expand D variable' do
          _(runner_variables.find { |v| v[:key] == 'D' }.fetch(:value)).must_equal '$BUILD_DIR'
        end

        it 'must not expand E variable' do
          _(runner_variables.find { |v| v[:key] == 'E' }.fetch(:value)).must_equal 'key%variable%%variable2%'
        end
      end

      it 'must not mark F variable as masked' do
        _(runner_variables.find { |v| v[:key] == 'F' }.fetch(:masked)).must_equal false
      end

      it 'must not mark G variable as masked' do
        _(runner_variables.find { |v| v[:key] == 'G' }.fetch(:masked)).must_equal false
      end

      it 'must not expand G variable' do
        _(runner_variables.find { |v| v[:key] == 'G' }.fetch(:value)).must_equal '$H'
      end
  end

    # describe 'with feature flag disabled' do
    #   before do
    #     $var_expansion_feature_flag = false
    #   end

    #   describe 'resolved $A' do
    #     let(:item) { @vars.find_key('A') }

    #     it 'matches unexpanded value' do
    #       _(item.resolved_value).must_equal item.value
    #     end
    #   end

    #   describe 'resolved $B' do
    #     let(:item) { @vars.find_key('B') }

    #     it 'matches unexpanded value' do
    #       _(item.resolved_value).must_equal item.value
    #     end
    #   end

    #   describe 'resolved $C' do
    #     let(:item) { @vars.find_key('C') }

    #     it 'matches unexpanded value' do
    #       _(item.resolved_value).must_equal item.value
    #     end
    #   end
    # end

    # describe 'with feature flag enabled' do
    #   before do
    #     $var_expansion_feature_flag = true
    #   end

    #   describe 'resolved $A' do
    #     before { @var_name = 'A' }
    #     let(:item) { @vars.find_key(@var_name) }

    #     it 'is expanded' do
    #       _(item.resolved_value).must_equal '1'
    #     end

    #     describe 'as raw variable' do
    #       before { @vars.find_key(@var_name).raw = true }

    #       it 'matches unexpanded value' do
    #         _(item.resolved_value).must_equal item.value
    #       end
    #     end
    #   end

    #   describe 'resolved $B' do
    #     before { @var_name = 'B' }
    #     let(:item) { @vars.find_key(@var_name) }

    #     it 'is expanded' do
    #       _(item.resolved_value).must_equal '1'
    #     end

    #     describe 'as raw variable' do
    #       before { @vars.find_key(@var_name).raw = true }

    #       it 'matches unexpanded value' do
    #         _(item.resolved_value).must_equal item.value
    #       end
    #     end
    #   end

    #   describe 'resolved $C' do
    #     before { @var_name = 'C' }
    #     let(:item) { @vars.find_key(@var_name) }

    #     it 'is expanded' do
    #       _(item.resolved_value).must_equal '1-1'
    #     end

    #     describe 'as raw variable' do
    #       before { @vars.find_key(@var_name).raw = true }

    #       it 'matches unexpanded value' do
    #         _(item.resolved_value).must_equal item.value
    #       end
    #     end
    #   end
    # end

    # describe 'resolved $D' do
    #   before { @var_name = 'D' }
    #   let(:item) { @vars.find_key(@var_name) }

    #   it 'matches unexpanded value' do
    #     _(item.resolved_value).must_equal item.value
    #   end

    #   describe 'as raw variable' do
    #     before { @vars.find_key(@var_name).raw = true }

    #     it 'matches unexpanded value' do
    #       _(item.resolved_value).must_equal item.value
    #     end
    #   end
    # end

    # describe 'resolved $E' do
    #   before { @var_name = 'E' }
    #   let(:item) { @vars.find_key(@var_name) }

    #   it 'matches unexpanded value' do
    #     _(item.resolved_value).must_equal item.value
    #   end

    #   describe 'as raw variable' do
    #     before { @vars.find_key(@var_name).raw = true }

    #     it 'matches unexpanded value' do
    #       _(item.resolved_value).must_equal item.value
    #     end
    #   end
    # end
  end

  describe 'with variable set containing multi-level indirection' do
    before do
      @vars = Variables.new(
        [
          { key: 'D', value: '$C-$A' },
          { key: 'C', value: 'test-$A-$B' },
          { key: 'A', value: '1' },
          { key: 'B', value: '2' }
        ]
      )
    end

    describe '#to_runner_variables' do
      let(:runner_variables) { @vars.to_runner_variables }

      describe 'with feature flag enabled' do
        before do
          $var_expansion_feature_flag = true
        end

        it 'must respond with original order (A B C D)' do
          _(runner_variables.length).must_equal 4
          _(runner_variables.map { |v| v[:key] }).must_equal %w[A B C D]
        end

        it 'must expand C variable' do
          _(runner_variables.find { |v| v[:key] == 'C' }.fetch(:value)).must_equal 'test-1-2'
        end

        it 'must expand D variable' do
          _(runner_variables.find { |v| v[:key] == 'D' }.fetch(:value)).must_equal 'test-1-2-1'
        end
      end
    end

    # describe 'with feature flag enabled' do
    #   before do
    #     $var_expansion_feature_flag = true
    #   end

    #   describe 'resolved $A' do
    #     before { @var_name = 'A' }
    #     let(:item) { @vars.find_key(@var_name) }

    #     it 'is expanded' do
    #       _(item.resolved_value).must_equal '1'
    #     end

    #     describe 'as raw variable' do
    #       before { @vars.find_key(@var_name).raw = true }

    #       it 'matches unexpanded value' do
    #         _(item.resolved_value).must_equal item.value
    #       end
    #     end
    #   end

    #   describe 'resolved $B' do
    #     before { @var_name = 'B' }
    #     let(:item) { @vars.find_key(@var_name) }

    #     it 'is expanded' do
    #       _(item.resolved_value).must_equal '2'
    #     end

    #     describe 'as raw variable' do
    #       before { @vars.find_key(@var_name).raw = true }

    #       it 'matches unexpanded value' do
    #         _(item.resolved_value).must_equal item.value
    #       end
    #     end
    #   end

    #   describe 'resolved $C' do
    #     before { @var_name = 'C' }
    #     let(:item) { @vars.find_key(@var_name) }

    #     it 'is expanded' do
    #       _(item.resolved_value).must_equal 'test-1-2'
    #     end

    #     describe 'as raw variable' do
    #       before { @vars.find_key(@var_name).raw = true }

    #       it 'matches unexpanded value' do
    #         _(item.resolved_value).must_equal item.value
    #       end
    #     end
    #   end
    # end

    # describe 'resolved $D' do
    #   before { @var_name = 'D' }
    #   let(:item) { @vars.find_key(@var_name) }

    #   it 'is expanded' do
    #     _(item.resolved_value).must_equal 'test-1-2-1'
    #   end

    #   describe 'as raw variable' do
    #     before { @vars.find_key(@var_name).raw = true }

    #     it 'matches unexpanded value' do
    #       _(item.resolved_value).must_equal item.value
    #     end
    #   end
    # end
  end
end
