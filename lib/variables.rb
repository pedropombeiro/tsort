# frozen_string_literal: true

require 'tsort'
require_relative 'variable'

$var_expansion_feature_flag = true

# Variables represents a collection of job variables
class Variables
  include Enumerable

  def initialize(variables = [])
    @variables = []

    variables.each { |variable| append(variable) }
  end

  def append(resource)
    tap { @variables.append(Variable.fabricate(resource)) }
  end

  def each
    @variables.each { |variable| yield variable }
  end

  def to_runner_variables
    runner_vars = @variables.map(&:to_runner_variable)
    return runner_vars unless $var_expansion_feature_flag

    # Perform a topological sort and inline expansion on a variable hash
    begin
      input_vars = runner_vars.map { |env| [env.fetch(:key), env] }.to_h
      sorted_var_values = {}
      each_node = ->(&b) { input_vars.each_key(&b) }
      each_child = ->(k, &b) { ExpandVariables.each_variable_reference(input_vars[k][:value], input_vars, &b) }

      TSort.tsort(each_node, each_child).map do |key|
        env = input_vars.fetch(key)
        value = env[:value]
        new_value = ExpandVariables.expand_existing(value, sorted_var_values)

        ensure_masked(env, input_vars) if new_value != value

        sorted_var_values.store(key, new_value)
        env.merge({ value: new_value })
      end
    rescue TSort::Cyclic => e
      raise CyclicVariableReference, e.message
    end
  end

  def ensure_masked(env, input_vars)
    # Mark variable as masked if it references masked variables
    return if env[:masked]

    ExpandVariables.each_variable_reference(env[:value], input_vars) do |ref_var_key|
      if input_vars.dig(ref_var_key, :masked)
        env[:masked] = true
        break
      end
    end
  end
end

# CyclicVariableReference is raised if a cyclic dependency is detected
CyclicVariableReference = Class.new(StandardError)
