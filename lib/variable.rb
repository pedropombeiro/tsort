# frozen_string_literal: true

require_relative 'expand_variables'

# Variable represents a job variable
class Variable
  def initialize(key:, value:, public: true, file: false, masked: false, raw: false)
    raise ArgumentError, "`#{key}` must be of type String or nil value, while it was: #{value.class}" unless
      value.is_a?(String) || value.nil?

    @variable = {
      key: key, value: value, public: public, file: file, masked: masked, raw: raw
    }
  end

  def [](key)
    @variable.fetch(key)
  end

  def ==(other)
    to_runner_variable == self.class.fabricate(nil, other).to_runner_variable
  end

  ##
  # If `file: true` has been provided we expose it, otherwise we
  # don't expose `file` attribute at all (stems from what the runner
  # expects).
  #
  def to_runner_variable
    @variable.reject { |hash_key, hash_value| hash_key == :file && hash_value == false }
  end

  def self.fabricate(resource)
    case resource
    when Hash
      self.new(**resource.transform_keys(&:to_sym))
    when ::Ci::HasVariable
      self.new(**resource.to_runner_variable)
    when self
      resource.dup
    else
      raise ArgumentError, "Unknown `#{resource.class}` variable resource!"
    end
  end
end
